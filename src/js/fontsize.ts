function get_ls_zoom(): number {
    let ls_zoom = localStorage.getItem('zoom');
    let zoom_temp = 0;
    if (ls_zoom != null) {
        zoom_temp = parseFloat(ls_zoom);
        if (Number.isNaN(zoom_temp)) {
            zoom_temp = 0;
        }
    }
    return zoom_temp;
}

function set_ls_zoom(zoom: number) {
    localStorage.setItem('zoom', zoom.toString());
}

function set_css_zoom(zoom: number) {
    document.documentElement.style.setProperty('--zoom', `${zoom}mm`);
}

function increase_font_size() {
    let zoom = get_ls_zoom();
    zoom += 0.1;
    set_ls_zoom(zoom);
    set_css_zoom(zoom);
}

function decrease_font_size() {
    let zoom = get_ls_zoom();
    zoom -= 0.1;
    set_ls_zoom(zoom);
    set_css_zoom(zoom);
}

function reset_font_size() {
    let zoom = 0;
    set_ls_zoom(zoom);
    set_css_zoom(zoom);
}

const on_click_decrease_font_size = document.querySelector('.on_click_decrease_font_size') as HTMLElement;
if (on_click_decrease_font_size) {
    on_click_decrease_font_size.addEventListener('click', decrease_font_size);
}
const on_click_reset_font_size = document.querySelector('.on_click_reset_font_size') as HTMLElement;
if (on_click_reset_font_size) {
    on_click_reset_font_size.addEventListener('click', reset_font_size);
}
const on_click_increase_font_size = document.querySelector('.on_click_increase_font_size') as HTMLElement;
if (on_click_increase_font_size) {
    on_click_increase_font_size.addEventListener('click', increase_font_size);
}

let zoom = get_ls_zoom();
set_css_zoom(zoom);
