# License

All code, if not marked differently, is licensed under MIT License.
See the file `MIT` for details.
All content, if not marked differently, is licensed unter Creative Commons Attribution 4.0 International (CC-BY-4.0).
See <https://creativecommons.org/licenses/by/4.0/legalcode> for details.

* Copyright (c) 2023 Leonhard Küper alias mogoh.
