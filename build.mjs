#!/usr/bin/env zx
import browserSync from "browser-sync";

async function clean() {
    await fs.remove(`${__dirname}/dist`);
    await fs.remove(`${__dirname}/src/css/@fontsource`);
}

async function css(mode, watch) {
    try {
        await fs.ensureSymlink(
            `${__dirname}/node_modules/@fontsource`,
            `${__dirname}/src/css/@fontsource`
        );    
    } catch (err) {
        console.log(err);
    }
    let flags = [
        `${__dirname}/src/**/*.css`,
        "--base",
        "src",
        "--verbose",
        "--dir",
        `${__dirname}/dist`,
    ];
    if (mode === "production") {
        flags = [...flags, "--env", "production"];
    }
    if (watch) {
        flags = [...flags, "--watch"];
    }
    await $`${__dirname}/node_modules/.bin/postcss ${flags}`;
}

async function server() {
    browserSync.init({
        files: ["*.css", "*.js"],
        watch: true,
        server: __dirname,
        port: 8080,
        open: false,
        index: "/example/index.html",
    });
}

function rollup(isProduction = true, watch) {
    let flags = ["--config"];
    if (isProduction) {
        flags = [...flags, "--environment", "NODE_ENV:production"];
    }
    if (watch) {
        flags = [...flags, "--watch"];
    }
    $`${__dirname}/node_modules/.bin/rollup ${flags}`;
}

const ZERO_ARGUMENTS = Object.keys(argv).length === 1 && argv["_"].length === 0;

if (argv.h || argv.help || ZERO_ARGUMENTS) {
    echo(`
    --clean, -c     Clean the output directory.
    --css, -p       Build the CSS files with PostCSS.
    --js, -j        Rollup JavaScript.
    --serve, -s     Start a dev server.
    --dev, -d       Development mode. Default: Production mode.
    --watch, -w     Watch for changes.
`);
    process.exit(0);
}

if (argv.clean || argv.c) {
    await clean();
}

if (argv.js || argv.j) {
    let watch = false;
    if (argv.watch || argv.w) {
        watch = true;
    }
    if (argv.dev || argv.d) {
        rollup(false, watch);
    } else {
        rollup(true, watch);
    }
}

if (argv.serve || argv.s) {
    server();
}

if (argv.css || argv.p) {
    let watch = false;
    if (argv.watch || argv.w) {
        watch = true;
    }
    if (argv.dev || argv.d) {
        await css("development", watch);
    } else {
        await css("production", watch);
    }
}
