import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import multiInput from 'rollup-plugin-multi-input';


const isProduction = process.env.NODE_ENV === 'production'


const dev = {
    input: [ './src/js/**/*.ts' ],
    output: {
        dir: 'dist/',
        format: 'esm',
        entryFileNames: '[name].js',
        sourcemap: 'inline',
    },
    plugins: [
        multiInput.default({
            relative: 'src/',
        }),
        nodeResolve(),
        typescript(),
        resolve(),
    ],
    watch: {
        exclude: ['node_modules/**'],
    },
}

const prod = {
    input: [ './src/js/**/*.ts' ],
    output: {
        dir: 'dist/',
        format: 'esm',
        sourcemap: false,
        compact: true,
        minifyInternalExports: true,
        entryFileNames: '[name].js',
    },
    plugins: [
        multiInput.default({
            relative: 'src/',
        }),
        nodeResolve(),
        typescript({
            inlineSourceMap: false,
        }),
        resolve(),
        terser(),
    ],
}

export default isProduction
    ? prod
    : dev;
