# README

Just some CSS, compiled with this technologies:

* <https://postcss.org/>
* <https://nodejs.org/>
* <https://yarnpkg.com/>
* <https://fontsource.org/>

Inspired by [Hello CSS](https://github.com/arp242/hello-css)

## Build

Requires `node` and `yarn` or similar.

```bash
git clone git@gitlab.com:mogoh/ramphis-css.git
cd ramphis-css
yarn install
```

```bash
yarn build --help
yarn build --clean
yarn build --clean --css --js --serve --dev --watch
yarn build -cpjsdw
```
